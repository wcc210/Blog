Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  resources :categories
  resources :articles do
    resources :comments
  end
  resources :users

  root 'homes#index'
  get 'about' => 'home#about'
  get 'category' => 'categories#index'

  get 'signup' => 'users#new'
  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  # admin page
  namespace :admin do
    root to: "articles#index"
    resources :articles
    resources :users
    resources :categories
  end

  #UploadsController
  post 'uploads' => 'uploads#create'
end
