I18n.load_path += Dir[Rails.root.join('config', 'locales', '*.{rb,yml}')]

# 应用可用的区域设置白名单
I18n.available_locales = [:en, :zh_cn]

# 修改默认区域设置（默认是 :en）
I18n.default_locale = :zh_cn
