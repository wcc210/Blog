class Article < ApplicationRecord
  belongs_to :user
  belongs_to :category

  # 如果文章删除的话，相关的评论也要删除
  has_many :comments, dependent: :destroy

  validates :content, :title, presence: true
  validates :title, uniqueness: true

  searchkick
end

Article.reindex



