class Admin::HomesController < ApplicationController
  layout "admin"

  include AdminHelper
  before_action :admin_user, only: [:index]

  def index

  end
end

