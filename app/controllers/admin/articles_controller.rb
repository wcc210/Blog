class Admin::ArticlesController < ApplicationController
  layout "admin"

  include AdminHelper
  before_action :admin_user, only: [:index]

  def index
    @articles = Article.paginate(page: params[:page], per_page: 4)
  end
end
