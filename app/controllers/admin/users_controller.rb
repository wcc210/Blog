class Admin::UsersController < ApplicationController
  layout "admin"

  include AdminHelper
  before_action :admin_user, only: [:index]
  def index
    @users = User.paginate(page: params[:page], per_page: 4)
  end
end
