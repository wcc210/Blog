class Admin::CategoriesController < ApplicationController
  layout "admin"

  include AdminHelper
  before_action :admin_user, only: [:index]

  def index
    @categories = Category.paginate(page: params[:page], per_page: 4)
  end
end


