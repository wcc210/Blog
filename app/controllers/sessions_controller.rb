# coding: utf-8
class SessionsController < ApplicationController
 # before_action :logged_in_user, only: [:new, :create, :destroy] # 只有登录才能删除用户

  skip_before_action :authorize, only: [:new, :create, :destroy]

  def new
  end

  def create
    user = User.find_by(email: params[:session][:email].downcase)
    if user && user.authenticate(params[:session][:password])
      # 登录成功
      flash[:danger] = "登录成功"
      log_in user
      params[:session][:remember_me] == '1' ? remember(user) : forget(user)
      # remember user
      # redirect_to index_path
      redirect_back_or user
    else
      # 返回登录界面
      flash.now[:danger] = '非法的邮箱和密码'
      render 'new'
    end
  end

  # 登出
  def destroy
    log_out if logged_in?
    redirect_to articles_path
  end
end
