class HomesController < ApplicationController
  include CurrentArticle
  include ArticlesHelper
  include ApplicationHelper

  skip_before_action :authorize, only: [:index]

  def index
    @categories = Category.order(:name).includes(:articles)
    if params[:q].present?
      @articles = Article.search(params[:q])
    else
      @articles = Article.order('created_at')
    end
    @articles = @articles.paginate(page: params[:page], per_page: 4)
  end

  def about
    @my_email = "gogkatsu@outlook.com"
    @github_address = "https://github.com/JackLovel"
  end

  def category

  end
end
