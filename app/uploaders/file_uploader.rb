# app/uploaders/file_uploader.rb
class FileUploader < CarrierWave::Uploader::Base
  def store_dir
    "uploads"
  end

  def filename
    "files/#{Time.now.strftime("%Y%m")}/#{secure_token}.#{file.extension}" if original_filename.present?
  end

  protected
  # Generate a random key like ActiveStoreage
  def secure_token
    return SecureRandom.base58(32) if model.nil?
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) || model.instance_variable_set(var, SecureRandom.base58(32))
  end
end
