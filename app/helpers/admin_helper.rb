module AdminHelper
  def admin_user
    unless admin_user?
      flash[:danger] = "没有访问的权限"
      redirect_to root_path
    end
  end
end
