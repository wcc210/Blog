module ArticlesHelper
  def create_time(article)
    # article.created_at.strftime("%Y-%m-%d %H:%M:%S")
    article.created_at.strftime("%Y-%m-%d")
  end

  def edit_time(article)
    # article.updated_at.strftime("%Y-%m-%d %H:%M:%S")
    article.updated_at.strftime("%Y-%m-%d")
  end

  #def author(article)
  #  if article.present?
  #    User.find_by_id(article.user_id).name
  #  end
  #end

  #def md_to_text(article)
  #  html_doc = markdown(article.content)
  #  text = strip_tags(html_doc)
  #end
end
