module UsersHelper
  # 返回指定用户的头像
  #def gravatar_for(user, options = { size: 80} )
  #  gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
  #  size = options[:size]
  #  gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
  #  image_tag(gravatar_url, alt: user.name, class: "gravatar")
  #end

  def gravatar_for(user)
    gravatar_url =  user.avatars[0].present? ? user.avatars[0].url : 'default_avatar.png'
    image_tag(gravatar_url,  size: "50x50", alt: user.name, class: "gravatar")
  end
end
