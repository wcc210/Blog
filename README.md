## quick start 
- checkout passenger and nginx install?
- checkout nginx.conf (such below `nginx config file` config)
- `don't use root permission` to download source code 
```
$ git clone https://github.com/JackLovel/railsdemoali.git
$ bundle install --without=development // 不使用开发环境中的包
```

- [生产环境中数据库生成及迁移](https://github.com/JackLovel/railsdemoali/blob/dev-for-windows/README.md#生产环境中数据库生成及迁移)
- start passenger 
```
$ touch tmp/restart.txt
```
- If there is an error, please read the two files `railsdemoali/log/production.log` and `nginx/logs/error.log` carefully

## version
```
ruby: 2.6.5
rails: 5.2.3
nginx: 1.17.3
Phusion Passenger: 6.0.4
source: https://gems.ruby-china.com/
```

## nginx config file
```
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    # 下面两行是自动生成的
    passenger_root /usr/local/rvm/gems/ruby-2.6.3/gems/passenger-6.0.4;
    passenger_ruby /usr/local/rvm/gems/ruby-2.6.3/wrappers/ruby;

    include       mime.types;
    default_type  application/octet-stream;

    sendfile        on;
    keepalive_timeout  65;


    server {
        listen       80;
        server_name  # 填入你的域名
        root /home/deploy/demo/public; # 填入项目的路径，例如：我的项目 demo 的路径是 /home/deploy/demo/
        passenger_enabled on;
        rails_env production; # 开启生产环境
    }
}
```

## 生产环境中数据库生成及迁移
- generate secret key 
```
$ bundle exec rake secret
```
- put secret key to `config/secrets.yml` and give some permission this article (chmod 755 config/secrets.yml)
```
production:
  secret_key_base: put secret key string in here
```
- migration database 
```
$ bundle exec rake assets:precompile db:migrate RAILS_ENV=production
```

# FAQ
## What is the current rails environment？
```
$ rails c 
> Rails.env
```
## log 文件
```
nginx 下的 logs 目录
app 项目下 log 目录 
```

## Could not find a JavaScript runtime.
```
$ apt install nodejs
```

## [wiki](https://github.com/JackLovel/railsdemoali/wiki) 
