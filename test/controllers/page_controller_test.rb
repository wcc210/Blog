require 'test_helper'

class PageControllerTest < ActionDispatch::IntegrationTest
  test "should get render_404" do
    get page_render_404_url
    assert_response :success
  end

  test "should get render_50Y0" do
    get page_render_50Y0_url
    assert_response :success
  end

end
