sudo apt update
sudo apt install -y nodejs  libpq-dev openjdk-8-jdk

# install elasticsearch (6.4.1版本), 非 root 下运行
wget https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.4.1.tar.gz
tar xvf elasticsearch-6.4.1.tar.gz
cd elasticsearch-6.4.1
./bin/elasticsearch -d # 后台运行